/************************************************
 * INF-746 - IoT and Cloud
 * Author......: Nicolai Ito 
 * Description.: Water Heating System
 ************************************************/
#include "rgb_lcd.h"
#include <Servo.h>
#include <math.h>
#include <Wire.h>
#include <TH02_dev.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "jsmn.h"
#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"

const int pinMoisture = A1;
const int pinButton = 0;
const int pinRelay = 5;
const int pinServo = 6;

#define MAX_MESSAGE_LENGTH 100
char mMessage[MAX_MESSAGE_LENGTH];
int mMsgSize = 0;

#define COMMAND_UNKNOWN 0
#define COMMAND_SET_GAS_COST 1
#define COMMAND_SET_ELETRIC_COST 2
#define COMMAND_SET_TARGET_TEMPERATURE 3
#define COMMAND_SET_REMOTE_BUTTON_STATE 4
#define COMMAND_SET_LOCAL_BUTTON_STATE 5
#define COMMAND_SET_WATER_TEMPERATURE 6
#define COMMAND_SET_MOISTURE_LEVEL 7
int mCommand = COMMAND_UNKNOWN;
float mCmdValue = 0;

#define TANK_OFF 0
#define TANK_GAS 1
#define TANK_ELETRIC 2
int mTankState = TANK_OFF;

float mCostGas = 0.8;
float mCostEletric = 1.2;
float mTempTarget = 30.2;
float mTempCurrent = 20.5;
bool mEnableSystem = false;
int mMoistureLevel = 345;
char mSystemStatus = 'O';

#define SERVO_POS_OFF 90
#define SERVO_POS_GAS 0
#define SERVO_POS_ELETRIC 179
Servo mServo;

rgb_lcd lcd;

long mButtonDebounce = 0;

IoT_Error_t rc = SUCCESS;
float mButtonState = 0;

void setup() {
  // Serial setup
  Serial.begin(9600);
  Serial.println("==== SETUP - BEGIN");

  // LCD
  lcd.begin(16, 2);
  Serial.println("LCD OK");

  // Button
  mButtonDebounce = millis();
  pinMode(pinButton,INPUT);
  attachInterrupt(pinButton, EncoderButton, FALLING);    
  Serial.println("Button OK");

  // Relay
  pinMode(pinRelay,OUTPUT);
  digitalWrite(pinRelay, LOW);
  Serial.println("Relay OK");

  // Moisture
  pinMode(pinMoisture, INPUT);
  Serial.println("Moisture OK");

  // Servo
  mServo.attach(pinServo);
  mServo.write(SERVO_POS_OFF);
  Serial.println("Moisture OK");

  // Internal values
  Serial.print("\nGas cost......: ");
  Serial.print(mCostGas);
  Serial.println("US$/kWh");

  Serial.print("Eletric cost..: ");
  Serial.print(mCostEletric);
  Serial.println("US$/kWh");

  Serial.print("Current temp..: ");
  Serial.print(mTempCurrent);
  Serial.println("C");

  Serial.print("Target temp...: ");
  Serial.print(mTempTarget);
  Serial.println("C");

  Serial.print("Moisture level: ");
  Serial.println(mMoistureLevel);

  // AWS
  Serial.println("\n== AWS Setup");
  rc = awsSetup();
  Serial.print("AWS state: ");
  Serial.println(rc);

  Serial.println("== Starting system\n");
  updateLCD();
  Serial.println("==== SETUP - END");
  Serial.println("Waiting for command.");
}

void loop() {
  while (getSerialCommand()) {
    Serial.print("Command: '");
    Serial.print(mMessage);
    Serial.println("'");

    // parse and execute command
    if (parseCommand()) {
      if (executeCommand()) {
        updateStates();
      } else {
        Serial.println("Could not execute command.");
      }
    } else {
      Serial.println("Invalid command. Command format: <action> <space> <value> <EOL>");
    }
    Serial.println("\nWaiting for command.");
  }

  // Update moisture level from sensor and update system
  if (mEnableSystem) {
    int moistureLevel = analogRead(pinMoisture);
    if (moistureLevel != mMoistureLevel) {
      Serial.print("Updating moisture level to ");
      Serial.println(moistureLevel);
      mMoistureLevel = moistureLevel;
      updateStates();
    }

    float temper = TH02.ReadTemperature();
    if (temper != mTempCurrent) {
      Serial.print("Updating current temperature to ");
      Serial.println(temper);
      mTempCurrent = temper;
      updateStates();
    }
  }

  sendState();
  delay(1000);
}

bool getSerialCommand() {
  mMsgSize = 0;
  // Wait for serial command
  while (Serial.available()) {
    mMessage[mMsgSize] = Serial.read();
    if ((mMessage[mMsgSize] == '\n' && mMsgSize > 0) || (++mMsgSize>= MAX_MESSAGE_LENGTH - 1) ) {
      mMessage[mMsgSize] = '\0';
      return true;
    }
  }
  return false;
}

bool parseCommand() {
  // get command
  mCommand = COMMAND_UNKNOWN;
  if (mMsgSize > 2 && mMessage[1] == ' ') {
    switch (mMessage[0]) {
      case 'G':
        mCommand = COMMAND_SET_GAS_COST;
        break;
      case 'E':
        mCommand = COMMAND_SET_ELETRIC_COST;
        break;
      case 'T':
        mCommand = COMMAND_SET_TARGET_TEMPERATURE;
        break;
      case 'R':
        mCommand = COMMAND_SET_REMOTE_BUTTON_STATE;
        break;
      case 'L':
        mCommand = COMMAND_SET_LOCAL_BUTTON_STATE;
        break;
      case 'W':
        mCommand = COMMAND_SET_WATER_TEMPERATURE;
        break;
      case 'M':
        mCommand = COMMAND_SET_MOISTURE_LEVEL;
        break;
      default:
        return false;
        break;
    }
  } else {
    return false;
  }

  // Get command value parameter
  mCmdValue = 0;
  int index = 2;
  bool hasDecimal = false;
  // get integer part
  for (; index < mMsgSize; index++) {
    if (mMessage[index] == '.') {
      hasDecimal = true;
      break;
    }
    if (mMessage[index] < '0' || mMessage[index] > '9') {
      return false;
    }
    mCmdValue = mCmdValue * 10 + (mMessage[index] - '0');
  }
  
  // get decimal value
  float decimal = 0;
  if (hasDecimal) {
    float base = 10;
    for (index++; index < mMsgSize; index++) {
      if (mMessage[index] < '0' || mMessage[index] > '9') {
        return false;
      }
      decimal += ((float)(mMessage[index] - '0')) / base;
      base *= 10;
    }
  }
  mCmdValue += decimal;
  return true;
}

bool executeCommand() {
  bool result = true;
  switch(mCommand) {
    case COMMAND_SET_GAS_COST:
      mCostGas = mCmdValue;
      Serial.print("Updating gas cost to ");
      Serial.print(mCostGas);
      Serial.println("US$/kWh");
      break;

    case COMMAND_SET_ELETRIC_COST:
      mCostEletric = mCmdValue;
      Serial.print("Updating eletric cost to ");
      Serial.print(mCostEletric);
      Serial.println("US$/kWh");
      break;

    case COMMAND_SET_TARGET_TEMPERATURE:
      mTempTarget = mCmdValue;
      Serial.print("Updating target temperature to ");
      Serial.print(mTempTarget);
      Serial.println("C");
      break;

    case COMMAND_SET_WATER_TEMPERATURE:
      mTempCurrent = mCmdValue;
      Serial.print("Updating current temperature to ");
      Serial.print(mTempCurrent);
      Serial.println("C");
      break;

    case COMMAND_SET_REMOTE_BUTTON_STATE:
    case COMMAND_SET_LOCAL_BUTTON_STATE:
      mEnableSystem = mCmdValue == 1;
      Serial.print("Turning system ON/OFF - is system ON: ");
      Serial.println(mEnableSystem);
      break;

    case COMMAND_SET_MOISTURE_LEVEL:
      mMoistureLevel = mCmdValue;
      Serial.print("Updating moisture level to ");
      Serial.println(mMoistureLevel);
      break;

    default:
      result = false;
      break;
  }
  return result;
}

void updateLCD() {
  lcd.clear();
  switch (mTankState) {
    case TANK_GAS:
      lcd.print(" G    ");
      mSystemStatus = 'G';
      break;
    case TANK_ELETRIC:
      lcd.print("  E   ");
      mSystemStatus = 'E';
      break;
    default:
      lcd.print("O     ");
      mSystemStatus = 'O';
      break;
  }
  lcd.print(mTempCurrent);
  lcd.print(" C");
  lcd.setCursor(6,1); 
  lcd.print(mTempTarget);
  lcd.print(" C");
}

void updateStates() {
  Serial.println("==== Updating system");

  // Update costs
  Serial.println("Updating costs");
  float factor = abs(1.0 - (mMoistureLevel / 1000.0) * 8.0);
  float newGasCost = mCostGas * factor;
  Serial.print("Gas cost......: ");
  Serial.print(newGasCost);
  Serial.println("US$/kWh");

  Serial.print("Eletric cost..: ");
  Serial.print(mCostEletric);
  Serial.println("US$/kWh");

  // Update system states according on costs and temperatures
  if (mEnableSystem) {
    if (mTempCurrent >= mTempTarget) {
      mTankState = TANK_OFF;
      mEnableSystem = false;
      Serial.println("System turning off - Reached target temperature");
    } else if (newGasCost < mCostEletric) {
      mTankState = TANK_GAS;
    } else {
      mTankState = TANK_ELETRIC;
    }
  } else {
    mTankState = TANK_OFF;
  }

  // Update relay state
  if (mEnableSystem) {
    digitalWrite(pinRelay, HIGH);
    mButtonState = 1;
  } else {
    digitalWrite(pinRelay, LOW);
    mButtonState = 0;
  }

  // Update servo state
  if (mTankState == TANK_GAS) {
    mServo.write(SERVO_POS_GAS);
  } else if (mTankState == TANK_ELETRIC) {
    mServo.write(SERVO_POS_ELETRIC);
  } else {
    mServo.write(SERVO_POS_OFF);
  }

  updateLCD();
  Serial.println("==== System updated\n");
}

void EncoderButton() {
  long curTime = millis();
  if (mButtonDebounce < curTime) {
    Serial.println("Button pressed - toogle system ON/OFF state");
    mButtonDebounce = curTime + 300;

    mCommand = COMMAND_SET_REMOTE_BUTTON_STATE;
    if (mEnableSystem) {
      mCmdValue = 0; 
    } else {
      mCmdValue = 1; 
    }
    executeCommand();
    updateStates();
  }
}

/**************************************************************************
 * Part 2 - Implementation
 **************************************************************************/
#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 1000

static char certDirectory[] = "/home/root/certs";
static char HostAddress[255] = AWS_IOT_MQTT_HOST;
static uint32_t port = AWS_IOT_MQTT_PORT;
static uint8_t numPubs = 5;

void ShadowUpdateStatusCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
                const char *pReceivedJsonDocument, void *pContextData) {
  IOT_UNUSED(pThingName);
  IOT_UNUSED(action);
  IOT_UNUSED(pReceivedJsonDocument);
  IOT_UNUSED(pContextData);

  if(SHADOW_ACK_TIMEOUT == status) {
    IOT_INFO("Update Timeout--");
  } else if(SHADOW_ACK_REJECTED == status) {
    IOT_INFO("Update RejectedXX");
  } else if(SHADOW_ACK_ACCEPTED == status) {
    IOT_INFO("Update Accepted !!");
  }
}

AWS_IoT_Client mqttClient;
char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);
char *pJsonStringToUpdate;

jsonStruct_t buttonHandler;
jsonStruct_t gasCostHandler;
jsonStruct_t eletricCostHandler;
jsonStruct_t targetTempHandler;
jsonStruct_t currentTempHandler;
jsonStruct_t systemStatusHandler;

void buttonCB(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.print("Button state is updated by cloud: ");
  Serial.println(mButtonState);
  mEnableSystem = mButtonState == 1;
  updateStates();
}

void gasCostCB(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.print("Gas cost is updated by cloud: ");
  Serial.println(mCostGas);
  updateStates();
}

void eletricCostCB(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.print("Gas cost is updated by cloud: ");
  Serial.println(mCostEletric);
  updateStates();
}

void targetTempCB(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.print("Target temperature is updated by cloud: ");
  Serial.println(mTempTarget);
  updateStates();
}

IoT_Error_t awsSetup() {
  IoT_Error_t rc = FAILURE;
  int32_t i = 0;

  buttonHandler.cb = buttonCB;
  buttonHandler.pKey = "remote_button";
  buttonHandler.pData = &mButtonState;
  buttonHandler.type = SHADOW_JSON_FLOAT;

  gasCostHandler.cb = gasCostCB;
  gasCostHandler.pData = &mCostGas;
  gasCostHandler.pKey = "gas_cost";
  gasCostHandler.type = SHADOW_JSON_FLOAT;

  eletricCostHandler.cb = eletricCostCB;
  eletricCostHandler.pData = &mCostEletric;
  eletricCostHandler.pKey = "eletric_cost";
  eletricCostHandler.type = SHADOW_JSON_FLOAT;

  targetTempHandler.cb = targetTempCB;
  targetTempHandler.pKey = "target_temperature";
  targetTempHandler.pData = &mTempTarget;
  targetTempHandler.type = SHADOW_JSON_FLOAT;

  currentTempHandler.cb = NULL;
  currentTempHandler.pKey = "water_temperature";
  currentTempHandler.pData = &mTempCurrent;
  currentTempHandler.type = SHADOW_JSON_FLOAT;

  systemStatusHandler.cb = NULL;
  systemStatusHandler.pKey = "water_heating_system_status";
  systemStatusHandler.pData = &mSystemStatus;
  systemStatusHandler.type = SHADOW_JSON_STRING;

  char rootCA[PATH_MAX + 1];
  char clientCRT[PATH_MAX + 1];
  char clientKey[PATH_MAX + 1];
  
  Serial.print("AWS IoT SDK Version ");
  Serial.print(VERSION_MAJOR);
  Serial.print(".");
  Serial.print(VERSION_MINOR);
  Serial.print(".");
  Serial.print(VERSION_PATCH);
  Serial.print("-");
  Serial.println(VERSION_TAG);

  snprintf(rootCA, PATH_MAX + 1, "%s/%s", certDirectory, AWS_IOT_ROOT_CA_FILENAME);
  snprintf(clientCRT, PATH_MAX + 1, "%s/%s", certDirectory, AWS_IOT_CERTIFICATE_FILENAME);
  snprintf(clientKey, PATH_MAX + 1, "%s/%s", certDirectory, AWS_IOT_PRIVATE_KEY_FILENAME);

  Serial.print("rootCA ");
  Serial.println(rootCA);
  Serial.print("clientCRT ");
  Serial.println(clientCRT);
  Serial.print("clientKey ");
  Serial.println(clientKey);

  // initialize the mqtt client
  ShadowInitParameters_t sp = ShadowInitParametersDefault;
  sp.pHost = AWS_IOT_MQTT_HOST;
  sp.port = AWS_IOT_MQTT_PORT;
  sp.pClientCRT = clientCRT;
  sp.pClientKey = clientKey;
  sp.pRootCA = rootCA;
  sp.enableAutoReconnect = false;
  sp.disconnectHandler = NULL;

  Serial.println("Shadow Init");
  rc = aws_iot_shadow_init(&mqttClient, &sp);
  if(SUCCESS != rc) {
    Serial.println(">>>>> Shadow Connection Error");
    return rc;
  }

  ShadowConnectParameters_t scp = ShadowConnectParametersDefault;
  scp.pMyThingName = AWS_IOT_MY_THING_NAME;
  scp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
  scp.mqttClientIdLen = (uint16_t) strlen(AWS_IOT_MQTT_CLIENT_ID);

  Serial.println("Shadow Connect");
  rc = aws_iot_shadow_connect(&mqttClient, &scp);
  if(SUCCESS != rc) {
    Serial.println(">>>>> Shadow Connection Error");
    return rc;
  }

  /*
   * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
   *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
   *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
   */
  rc = aws_iot_shadow_set_autoreconnect_status(&mqttClient, true);
  if(SUCCESS != rc) {
    Serial.print(">>>>> Unable to set Auto Reconnect to true - ");
    Serial.println(rc);
    return rc;
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &buttonHandler);
  if(SUCCESS != rc) {
    Serial.println(">>>>> Shadow Register Delta Button Error");
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &gasCostHandler);
  if(SUCCESS != rc) {
    Serial.println(">>>>> Shadow Register Delta Gas Cost Error");
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &eletricCostHandler);
  if(SUCCESS != rc) {
    Serial.println(">>>>> Shadow Register Delta Eletric Cost Error");
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &targetTempHandler);
  if(SUCCESS != rc) {
    Serial.println(">>>>> Shadow Register Delta Target Temperature Error");
  }
  return rc;
}

void sendState() {
  // loop and publish a change in temperature
  if (NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || SUCCESS == rc) {
    rc = aws_iot_shadow_yield(&mqttClient, 200);
    if(NETWORK_ATTEMPTING_RECONNECT == rc) {
      // If the client is attempting to reconnect we will skip the rest of the loop.
      return;
    }

    Serial.println("\n=======================================================================================");
    rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
    if(SUCCESS == rc) {
      rc = aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer,
                       6,
                       &buttonHandler,
                       &gasCostHandler,
                       &eletricCostHandler,
                       &targetTempHandler,
                       &currentTempHandler,
                       &systemStatusHandler);
      if(SUCCESS == rc) {
        rc = aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
        if(SUCCESS == rc) {
          IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
          Serial.print("Update Shadow: ");
          Serial.println(JsonDocumentBuffer);
          rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                         ShadowUpdateStatusCallback, NULL, 4, true);
        }
      }
    }
    Serial.println("=======================================================================================");
  }
}

