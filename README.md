INF-746 IoT and Cloud
Trabalho final - Water Heating System
Autor: Nicolai Ito


===================================
Setup
===================================
Certification files should be on folder "/home/root/certs"

===================================
Description
===================================
A house is equipped with a water heating system that uses three sources of heat: solar,
gas and electric. The solar system has the lowest US$/kWh of the three and because it is
owned by the house proprietor its US$/kWh is going to be considered constant and negli-
gible to the cost of running the water heating system. The solar heating system is always
on and usually is enough to maintain the water of the house at the desired temperature.
The gas and electric heating systems are used as backups and/or to supplement the heat
necessary on days where enough sun light is not available. The costs of the kWh of gas
and electricity vary in time, sometimes electricity is cheaper than gas, sometimes not,
both add significative costs to the cost of operation of the heating system. Thus, both
costs are considered parameters (inputs) of the water heating system.

